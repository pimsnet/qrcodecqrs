<?php

namespace PimsCQRS\Domain\Business;

use PimsCQRS\Infrastructure\CQRS\SimpleEventSourcedEntity;

abstract class QRCode extends SimpleEventSourcedEntity
{
    /** @var string */
    const USE_MANY = 'use_many';

    /** @var string */
    const USE_ONCE = 'use_once';

    /** @var array */
    const USE_TYPES = [
        [
            'name' => 'Use many',
            'slug' => self::USE_MANY
        ],
        [
            'name' => 'One time',
            'slug' => self::USE_ONCE
        ],
    ];

    /** @var string */
    private $qrCodeId;

    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $labelName;

    /** @var string */
    private $useType;

    /** @var bool */
    private $isEnabled;

    /**
     * @param string $qrCodeId
     * @param string $businessId
     * @param string $userId
     * @param string $labelName
     * @param string $useType
     * @param bool $isEnabled
     */
    public function __construct(
        string $qrCodeId,
        string $businessId,
        string $userId,
        string $labelName,
        string $useType,
        bool $isEnabled
    )
    {
        $this->qrCodeId = $qrCodeId;
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->labelName = $labelName;
        $this->useType = $useType;
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return string
     */
    public function getQrCodeId(): string
    {
        return $this->qrCodeId;
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    abstract public function enableQRCode(string $userId, string $qrCodeId);

    /**
     * @return void
     */
    public function enable()
    {
        $this->isEnabled = true;
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    abstract public function disableQRCode(string $userId, string $qrCodeId);

    /**
     * @return void
     */
    public function disable()
    {
        $this->isEnabled = false;
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    abstract public function updateQRCode(string $userId, string $qrCodeId, string $labelName, string $useType);

    /**
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function update(string $labelName, string $useType)
    {
        $this->labelName = $labelName;
        $this->useType = $useType;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'qrCodeId' => $this->qrCodeId,
            'businessId' => $this->businessId,
            'userId' => $this->userId,
            'labelName' => $this->labelName,
            'useType' => $this->useType,
            'isEnabled' => $this->isEnabled
        ];
    }

    /**
     * @param array $data
     *
     * @return mixed The object instance
     */
    public static function deserialize(array $data): self
    {
        return new static (
            $data['qrCodeId'],
            $data['businessId'],
            $data['userId'],
            $data['labelName'],
            $data['useType'],
            $data['isEnabled']
        );
    }
}
