<?php

namespace PimsCQRS\Domain\Business;

use PimsCQRS\Domain\Business\Event\BusinessCreatedEvent;
use PimsCQRS\Domain\Business\Exception\BusinessNotCreatedException;
use PimsCQRS\Domain\Business\Exception\BusinessNeedsToBeCreatedFirstException;
use PimsCQRS\Infrastructure\CQRS\EventSourcedAggregateRoot;

class Business extends EventSourcedAggregateRoot implements BusinessInterface
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $name;

    /** @var QRCodes */
    private $qrCodes;

    /**
     * @return string
     */
    public function getAggregateRootId()
    {
        return $this->businessId;
    }

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $name
     *
     * @return BusinessInterface
     */
    public static function createBusiness(string $businessId, string $userId, string $name)
    {
        $business = new self();
        $business->apply(new BusinessCreatedEvent($businessId, $userId, $name));

        return $business;
    }

    /**
     * @param BusinessCreatedEvent $event
     */
    public function applyBusinessCreatedEvent(BusinessCreatedEvent $event)
    {
        $this->businessId = $event->getBusinessId();
        $this->userId = $event->getUserId();
        $this->name = $event->getName();
        $this->qrCodes = new QRCodes();
    }


    /**
     * @return array|\PimsCQRS\Infrastructure\CQRS\EventSourcedEntity[]
     */
    protected function getChildEntities()
    {
        $childEntities = [];

        if (null !== $this->qrCodes) {
            $childEntities[] = $this->qrCodes;
        }

        return $childEntities;
    }

    /**
     * @param string $userId
     * @param StampCollectionQRCode $stampCollectionQRCode
     *
     * @return void
     */
    public function defineStampCollectionQRCode(string $userId, StampCollectionQRCode $stampCollectionQRCode)
    {
        if (empty($this->businessId)) {
            throw new BusinessNeedsToBeCreatedFirstException();
        }

        $this->qrCodes->defineStampCollectionQRCode($userId, $stampCollectionQRCode);
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function enableQRCode(string $userId, string $qrCodeId)
    {
        if (empty($this->businessId)) {
            throw new BusinessNotCreatedException();
        }

        $this->qrCodes->enableQRCode($userId, $qrCodeId);
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function disableQRCode(string $userId, string $qrCodeId)
    {
        if (empty($this->businessId)) {
            throw new BusinessNotCreatedException();
        }

        $this->qrCodes->disableQRCode($userId, $qrCodeId);
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function updateStampCollectionQRCode(string $userId, string $qrCodeId, string $labelName, string $useType)
    {
        if (empty($this->businessId)) {
            throw new BusinessNotCreatedException();
        }

        $this->qrCodes->updateQRCode($userId, $qrCodeId, $labelName, $useType);
    }
}
