<?php

namespace PimsCQRS\Domain\Business\Event;

use PimsCQRS\Domain\Event\SerializableDomainEvent;

class StampCollectionQRCodeEnabledEvent implements SerializableDomainEvent
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $qrCodeId;

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $qrCodeId
     */
    public function __construct(string $businessId, string $userId, string $qrCodeId)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->qrCodeId = $qrCodeId;
    }

    /**
     * @param array $data
     *
     * @return SerializableDomainEvent
     */
    public static function deserialize(array $data): SerializableDomainEvent
    {
        return new self(
            $data['businessId'],
            $data['userId'],
            $data['qrCodeId']
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'businessId' => $this->getBusinessId(),
            'userId' => $this->getUserId(),
            'qrCodeId' => $this->getQrCodeId()
        ];
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getQrCodeId(): string
    {
        return $this->qrCodeId;
    }
}