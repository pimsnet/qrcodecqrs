<?php

namespace PimsCQRS\Domain\Business\Event;

use PimsCQRS\Domain\Event\SerializableDomainEvent;

class StampCollectionQRCodeUpdatedEvent implements SerializableDomainEvent
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $qrCodeId;

    /** @var string */
    private $labelName;

    /** @var string */
    private $useType;

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     */
    public function __construct(string $businessId, string $userId, string $qrCodeId, string $labelName, string $useType)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->qrCodeId = $qrCodeId;
        $this->labelName = $labelName;
        $this->useType = $useType;
    }

    /**
     * @param array $data
     *
     * @return SerializableDomainEvent
     */
    public static function deserialize(array $data): SerializableDomainEvent
    {
        return new self(
            $data['businessId'],
            $data['userId'],
            $data['qrCodeId'],
            $data['labelName'],
            $data['useType']
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'businessId' => $this->getBusinessId(),
            'userId' => $this->getUserId(),
            'qrCodeId' => $this->getQrCodeId(),
            'labelName' => $this->getLabelName(),
            'useType' => $this->getUseType(),
        ];
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getQrCodeId(): string
    {
        return $this->qrCodeId;
    }

    /**
     * @return string
     */
    public function getLabelName(): string
    {
        return $this->labelName;
    }

    /**
     * @return string
     */
    public function getUseType(): string
    {
        return $this->useType;
    }
}