<?php

namespace PimsCQRS\Domain\Business\Event;

use PimsCQRS\Domain\Business\StampCollectionQRCode;
use PimsCQRS\Domain\Event\SerializableDomainEvent;

class StampCollectionQRCodeDefinedEvent implements SerializableDomainEvent
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var StampCollectionQRCode */
    private $stampCollectionQRCode;

    /**
     * @param string $businessId
     * @param string $userId
     * @param StampCollectionQRCode $stampCollectionQRCode
     */
    public function __construct(string $businessId, string $userId, StampCollectionQRCode $stampCollectionQRCode)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->stampCollectionQRCode = $stampCollectionQRCode;
    }

    /**
     * @param array $data
     *
     * @return SerializableDomainEvent
     */
    public static function deserialize(array $data): SerializableDomainEvent
    {
        return new self(
            $data['businessId'],
            $data['userId'],
            StampCollectionQRCode::deserialize($data['qrCode'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'businessId' => $this->getBusinessId(),
            'userId' => $this->getUserId(),
            'qrCode' => $this->getStampCollectionQRCode()->serialize()
        ];
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return StampCollectionQRCode
     */
    public function getStampCollectionQRCode(): StampCollectionQRCode
    {
        return $this->stampCollectionQRCode;
    }
}