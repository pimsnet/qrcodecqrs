<?php

namespace PimsCQRS\Domain\Business\Event;

use PimsCQRS\Domain\Event\SerializableDomainEvent;

class BusinessCreatedEvent implements SerializableDomainEvent
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $name;

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $name
     */
    public function __construct(string $businessId, string $userId, string $name)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->name = $name;
    }

    public static function deserialize(array $data): SerializableDomainEvent
    {
        return new self(
            $data['businessId'], $data['userId'], $data['name']
        );
    }

    public function serialize(): array
    {
        return [
            'businessId' => $this->getBusinessId(),
            'userId' => $this->getUserId(),
            'name' => $this->getName(),
        ];
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}