<?php

namespace PimsCQRS\Domain\Business\Exception;

use RuntimeException;

class QRCodeExistsException extends RuntimeException
{
    /** @var string */
    protected $message = 'QR Code already exists';
}
