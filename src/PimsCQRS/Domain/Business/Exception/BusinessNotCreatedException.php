<?php

namespace PimsCQRS\Domain\Business\Exception;

use RuntimeException;

class BusinessNotCreatedException extends RuntimeException
{
    /** @var string */
    protected $message = 'Business not created';
}