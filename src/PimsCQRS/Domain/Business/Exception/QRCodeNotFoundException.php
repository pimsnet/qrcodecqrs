<?php

namespace PimsCQRS\Domain\Business\Exception;

use RuntimeException;

class QRCodeNotFoundException extends RuntimeException
{
    /** @var string */
    protected $message = 'QR Code not found';
}
