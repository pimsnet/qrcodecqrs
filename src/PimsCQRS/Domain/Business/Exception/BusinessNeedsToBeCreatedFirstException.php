<?php

namespace PimsCQRS\Domain\Business\Exception;

class BusinessNeedsToBeCreatedFirstException extends \RuntimeException
{
    /** @var string */
    protected $message = 'Business needs to be created first.';
}