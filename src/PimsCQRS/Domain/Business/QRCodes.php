<?php

namespace PimsCQRS\Domain\Business;

use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDefinedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDisabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeEnabledEvent;
use PimsCQRS\Domain\Business\Exception\QRCodeExistsException;
use PimsCQRS\Domain\Business\Exception\QRCodeNotFoundException;
use PimsCQRS\Infrastructure\CQRS\SimpleEventSourcedEntity;

class QRCodes extends SimpleEventSourcedEntity
{
    /** @var QRCodes[] */
    private $values = [];

    /**
     * @param QRCode[] ...$qrCodes
     */
    public function __construct(QRCode ...$qrCodes)
    {
        $this->values = $qrCodes;
    }

    /**
     * @param QRCode $qrCode
     *
     * @return void
     */
    public function add(QRCode $qrCode)
    {
        try {
            $this->findByQRCodeId($qrCode->getQrCodeId());

            throw new QRCodeExistsException();
        } catch (QRCodeNotFoundException $exception) {
        }

        $this->values[] = $qrCode;
    }

    /**
     * @param string $rewardId
     *
     * @return bool
     */
    public function containsId(string $rewardId)
    {
        try {
            return false === empty($this->findByQRCodeId($rewardId));
        } catch (QRCodeNotFoundException $exception) {
            return false;
        }
    }

    /**
     * @param string $qrCodeId
     *
     * @return QRCode
     */
    public function findByQRCodeId(string $qrCodeId): QRCode
    {
        $results = array_filter(
            $this->values,
            function ($qrCode) use ($qrCodeId) {
                /** @var QRCode $qrCode */
                return $qrCode->getQrCodeId() == $qrCodeId;
            });

        if (true === empty($results)) {
            throw new QrCodeNotFoundException();
        }

        return array_shift($results);
    }

    /**
     * @param string $userId
     * @param StampCollectionQRCode $stampCollectionQRCode
     *
     * @return void
     */
    public function defineStampCollectionQRCode(string $userId, StampCollectionQRCode $stampCollectionQRCode)
    {
        $this->apply(
            new StampCollectionQRCodeDefinedEvent(
                $this->getAggregateRoot()->getAggregateRootId(),
                $userId,
                $stampCollectionQRCode
            )
        );
    }

    /**
     * @param StampCollectionQRCodeDefinedEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeDefinedEvent(StampCollectionQRCodeDefinedEvent $event)
    {
        $this->add(
            $event->getStampCollectionQRCode()
        );
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function enableQRCode(string $userId, string $qrCodeId)
    {
        $qrCode = $this->findByQRCodeId($qrCodeId);
        $qrCode->enableQRCode($userId, $qrCodeId);
    }

    /**
     * @param string $qrCodeId
     */
    public function enable(string $qrCodeId)
    {
        $qrCode = $this->findByQRCodeId($qrCodeId);
        $qrCode->enable();
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function disableQRCode(string $userId, string $qrCodeId)
    {
        $qrCode = $this->findByQRCodeId($qrCodeId);
        $qrCode->disableQRCode($userId, $qrCodeId);
    }

    /**
     * @param string $qrCodeId
     */
    public function disable(string $qrCodeId)
    {
        $qrCode = $this->findByQRCodeId($qrCodeId);
        $qrCode->disable();
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function updateQRCode(string $userId, string $qrCodeId, string $labelName, string $useType)
    {
        $qrCode = $this->findByQRCodeId($qrCodeId);
        $qrCode->updateQRCode($userId, $qrCodeId, $labelName, $useType);
    }

    /**
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function update(string $qrCodeId, string $labelName, string $useType)
    {
        $qrCode = $this->findByQRCodeId($qrCodeId);
        $qrCode->update($labelName, $useType);
    }

    /**
     * @return array
     */
    protected function getChildEntities()
    {
        return $this->values;
    }
}
