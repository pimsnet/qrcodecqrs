<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\SimpleCommandHandler;
use PimsCQRS\Domain\Business\Business;
use PimsCQRS\Domain\Business\BusinessRepository;

class EnableQRCodeCommandHandler extends SimpleCommandHandler
{
    /** @var BusinessRepository */
    private $repository;

    /**
     * @param BusinessRepository $repository
     */
    public function __construct(BusinessRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param EnableQRCodeCommand $command
     */
    public function handleEnableQRCodeCommand(EnableQRCodeCommand $command)
    {
        /** @var Business $business */
        $business = $this->repository->load($command->getBusinessId());

        $business->enableQRCode(
            $command->getUserId(),
            $command->getQrCodeId()
        );

        $this->repository->save($business);
    }
}