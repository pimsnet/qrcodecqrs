<?php

namespace PimsCQRS\Domain\Business\Command;

use PimsCQRS\Domain\Business\StampCollectionQRCode;

class CreateBusinessCommand
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $name;

    /** @var StampCollectionQRCode */
    private $stampCollectionQRCode;

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $name
     * @param StampCollectionQRCode $stampCollectionQRCode
     */
    public function __construct(string $businessId, string $userId, string $name, StampCollectionQRCode $stampCollectionQRCode)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->name = $name;
        $this->stampCollectionQRCode = $stampCollectionQRCode;
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return StampCollectionQRCode
     */
    public function getStampCollectionQRCode(): StampCollectionQRCode
    {
        return $this->stampCollectionQRCode;
    }
}