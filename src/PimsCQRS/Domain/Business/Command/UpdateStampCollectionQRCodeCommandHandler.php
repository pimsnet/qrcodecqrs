<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\SimpleCommandHandler;
use PimsCQRS\Domain\Business\Business;
use PimsCQRS\Domain\Business\BusinessRepository;

class UpdateStampCollectionQRCodeCommandHandler extends SimpleCommandHandler
{
    /** @var BusinessRepository */
    private $repository;

    /**
     * @param BusinessRepository $repository
     */
    public function __construct(BusinessRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UpdateStampCollectionQRCodeCommand $command
     */
    public function handleUpdateStampCollectionQrCodeCommand(UpdateStampCollectionQRCodeCommand $command)
    {
        /** @var Business $business */
        $business = $this->repository->load($command->getBusinessId());

        $business->updateStampCollectionQRCode(
            $command->getUserId(),
            $command->getQrCodeId(),
            $command->getLabelName(),
            $command->getUseType()
        );

        $this->repository->save($business);
    }
}