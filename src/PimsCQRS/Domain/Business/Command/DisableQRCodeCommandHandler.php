<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\SimpleCommandHandler;
use PimsCQRS\Domain\Business\Business;
use PimsCQRS\Domain\Business\BusinessRepository;

class DisableQRCodeCommandHandler extends SimpleCommandHandler
{
    /** @var BusinessRepository */
    private $repository;

    /**
     * @param BusinessRepository $repository
     */
    public function __construct(BusinessRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DisableQRCodeCommand $command
     */
    public function handleDisableQRCodeCommand(DisableQRCodeCommand $command)
    {
        /** @var Business $business */
        $business = $this->repository->load($command->getBusinessId());

        $business->disableQRCode(
            $command->getUserId(),
            $command->getQrCodeId()
        );

        $this->repository->save($business);
    }
}