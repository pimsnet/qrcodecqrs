<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\SimpleCommandHandler;
use PimsCQRS\Domain\Business\Business;
use PimsCQRS\Domain\Business\BusinessRepository;

class DefineStampCollectionQRCodeCommandHandler extends SimpleCommandHandler
{
    /** @var BusinessRepository */
    private $repository;

    /**
     * @param BusinessRepository $repository
     */
    public function __construct(BusinessRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DefineStampCollectionQRCodeCommand $command
     */
    public function handleDefineStampCollectionQRCodeCommand(DefineStampCollectionQRCodeCommand $command)
    {
        /** @var Business $business */
        $business = $this->repository->load($command->getBusinessId());

        $business->defineStampCollectionQRCode(
            $command->getUserId(),
            $command->getStampCollectionQRCode()
        );

        $this->repository->save($business);
    }
}