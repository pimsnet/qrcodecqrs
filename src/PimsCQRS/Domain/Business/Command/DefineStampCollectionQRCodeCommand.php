<?php

namespace PimsCQRS\Domain\Business\Command;

use PimsCQRS\Domain\Business\StampCollectionQRCode;

class DefineStampCollectionQRCodeCommand
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var StampCollectionQRCode */
    private $stampCollectionQRCode;

    /**
     * @param string $businessId
     * @param string $userId
     * @param StampCollectionQRCode $stampCollectionQRCode
     */
    public function __construct(string $businessId, string $userId, StampCollectionQRCode $stampCollectionQRCode)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->stampCollectionQRCode = $stampCollectionQRCode;
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return StampCollectionQRCode
     */
    public function getStampCollectionQRCode(): StampCollectionQRCode
    {
        return $this->stampCollectionQRCode;
    }
}