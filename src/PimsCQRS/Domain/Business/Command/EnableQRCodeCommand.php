<?php

namespace PimsCQRS\Domain\Business\Command;

class EnableQRCodeCommand
{
    /** @var string */
    private $businessId;

    /** @var string */
    private $userId;

    /** @var string */
    private $qrCodeId;

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $qrCodeId
     */
    public function __construct(string $businessId, string $userId, string $qrCodeId)
    {
        $this->businessId = $businessId;
        $this->userId = $userId;
        $this->qrCodeId = $qrCodeId;
    }

    /**
     * @return string
     */
    public function getBusinessId(): string
    {
        return $this->businessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getQrCodeId(): string
    {
        return $this->qrCodeId;
    }
}