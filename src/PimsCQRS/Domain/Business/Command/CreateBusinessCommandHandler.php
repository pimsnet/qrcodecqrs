<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\SimpleCommandHandler;
use PimsCQRS\Domain\Business\Business;
use PimsCQRS\Domain\Business\BusinessRepository;

class CreateBusinessCommandHandler extends SimpleCommandHandler
{
    /** @var BusinessRepository */
    private $repository;

    /**
     * @param BusinessRepository $repository
     */
    public function __construct(BusinessRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateBusinessCommand $command
     */
    public function handleCreateBusinessCommand(CreateBusinessCommand $command)
    {
        $business = Business::createBusiness(
            $command->getBusinessId(), $command->getUserId(), $command->getName()
        );

        $business->defineStampCollectionQRCode(
            $command->getUserId(),
            $command->getStampCollectionQRCode()
        );

        $this->repository->save($business);
    }
}