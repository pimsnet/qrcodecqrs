<?php

namespace PimsCQRS\Domain\Business;

use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDisabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeEnabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeUpdatedEvent;

class StampCollectionQRCode extends QRCode
{
    /**
     * @param string $userId
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function updateQRCode(string $userId, string $qrCodeId, string $labelName, string $useType)
    {
        $this->apply(
            new StampCollectionQRCodeUpdatedEvent(
                $this->getAggregateRoot()->getAggregateRootId(),
                $userId,
                $qrCodeId,
                $labelName,
                $useType
            )
        );
    }

    /**
     * @param StampCollectionQRCodeUpdatedEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeUpdatedEvent(StampCollectionQRCodeUpdatedEvent $event)
    {
        $this->update($event->getLabelName(), $event->getUseType());
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function disableQRCode(string $userId, string $qrCodeId)
    {
        $this->apply(
            new StampCollectionQRCodeDisabledEvent(
                $this->getAggregateRoot()->getAggregateRootId(),
                $userId,
                $qrCodeId
            )
        );
    }

    /**
     * @param StampCollectionQRCodeDisabledEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeDisabledEvent(StampCollectionQRCodeDisabledEvent $event)
    {
        $this->disable();
    }

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function enableQRCode(string $userId, string $qrCodeId)
    {
        $this->apply(
            new StampCollectionQRCodeEnabledEvent(
                $this->getAggregateRoot()->getAggregateRootId(),
                $userId,
                $qrCodeId
            )
        );
    }

    /**
     * @param StampCollectionQRCodeEnabledEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeEnabledEvent(StampCollectionQRCodeEnabledEvent $event)
    {
        $this->enable();
    }
}
