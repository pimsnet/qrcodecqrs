<?php

namespace PimsCQRS\Domain\Business;

interface BusinessInterface
{
    /**
     * @param string $businessId
     * @param string $userId
     * @param string $name
     *
     * @return BusinessInterface
     */
    public static function createBusiness(string $businessId, string $userId, string $name);

    /**
     * @param string $userId
     * @param StampCollectionQRCode $stampCollectionQRCode
     *
     * @return void
     */
    public function defineStampCollectionQRCode(string $userId, StampCollectionQRCode $stampCollectionQRCode);

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function enableQRCode(string $userId, string $qrCodeId);

    /**
     * @param string $userId
     * @param string $qrCodeId
     *
     * @return void
     */
    public function disableQRCode(string $userId, string $qrCodeId);

    /**
     * @param string $userId
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function updateStampCollectionQRCode(string $userId, string $qrCodeId, string $labelName, string $useType);
}