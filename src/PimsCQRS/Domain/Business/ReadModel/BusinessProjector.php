<?php

namespace PimsCQRS\Domain\Business\ReadModel;

use Broadway\Domain\DomainMessage;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Repository;
use PimsCQRS\Domain\Business\Event\BusinessCreatedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDisabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeEnabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeUpdatedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDefinedEvent;

class BusinessProjector extends Projector
{
    /** @var Repository */
    private $repository;

    /**
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param BusinessCreatedEvent $event
     * @param DomainMessage $domainMessage
     *
     * @return void
     */
    public function applyBusinessCreatedEvent(BusinessCreatedEvent $event, DomainMessage $domainMessage)
    {
        try {
            $this->repository->remove($event->getBusinessId());
        } catch (\Exception $exception) {

        }

        $business = Business::createBusiness(
            $event->getBusinessId(), $event->getUserId(), $event->getName(), $domainMessage->getRecordedOn()
        );

        $this->repository->save($business);
    }

    /**
     * @param StampCollectionQRCodeDefinedEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeDefinedEvent(StampCollectionQRCodeDefinedEvent $event)
    {
        /** @var Business $business */
        $business = $this->repository->find($event->getBusinessId());

        $business->defineStampCollectionQRCode($event->getStampCollectionQRCode());

        $this->repository->save($business);
    }

    /**
     * @param StampCollectionQRCodeEnabledEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeEnabledEvent(StampCollectionQRCodeEnabledEvent $event)
    {
        /** @var Business $business */
        $business = $this->repository->find($event->getBusinessId());

        $business->enableQRCode($event->getQrCodeId());

        $this->repository->save($business);
    }

    /**
     * @param StampCollectionQRCodeDisabledEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeDisabledEvent(StampCollectionQRCodeDisabledEvent $event)
    {
        /** @var Business $business */
        $business = $this->repository->find($event->getBusinessId());

        $business->disableQRCode($event->getQrCodeId());

        $this->repository->save($business);
    }

    /**
     * @param StampCollectionQRCodeUpdatedEvent $event
     *
     * @return void
     */
    public function applyStampCollectionQRCodeUpdatedEvent(StampCollectionQRCodeUpdatedEvent $event)
    {
        /** @var Business $business */
        $business = $this->repository->find($event->getBusinessId());

        $business->updateQrCode($event->getQrCodeId(), $event->getLabelName(), $event->getUseType());

        $this->repository->save($business);
    }

}
