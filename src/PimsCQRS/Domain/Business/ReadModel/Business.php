<?php

namespace PimsCQRS\Domain\Business\ReadModel;

use Broadway\Domain\DateTime;
use PimsCQRS\Domain\Business\QRCodes;
use PimsCQRS\Domain\Business\StampCollectionQRCode;

class Business implements BusinessInterface
{
    /** @var string */
    private $id;

    /** @var DateTime */
    public $createdAt;

    /** @var string */
    private $ownerId;

    /** @var string */
    private $name;

    /** @var QRCodes */
    private $qrCodes;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $data
     *
     * @return Business
     */
    public static function deserialize(array $data)
    {
        $business = new self();
        $business->id = $data['id'];
        $business->createdAt = (isset($data['createdAt'])) ? DateTime::fromString($data['createdAt']) : null;
        $business->ownerId = $data['ownerId'];
        $business->name = $data['name'];
        $business->qrCodes = $data['qrCodes'];

        return $business;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'id' => $this->id,
            'createdAt' => ($this->createdAt) ? $this->createdAt->toString() : null,
            'ownerId' => $this->ownerId,
            'name' => $this->name,
            'qrCodes' => $this->qrCodes
        ];
    }

    /**
     * @param string $businessId
     * @param string $userId
     * @param string $name
     * @param DateTime $dateTime
     *
     * @return Business
     */
    public static function createBusiness(string $businessId, string $userId, string $name, DateTime $dateTime)
    {
        $business = new self();

        $business->id = $businessId;
        $business->ownerId = $userId;
        $business->name = $name;
        $business->createdAt = $dateTime;
        $business->qrCodes = new QRCodes();

        return $business;
    }

    /**
     * @param StampCollectionQRCode $stampCollectionQRCode
     *
     * @return void
     */
    public function defineStampCollectionQRCode(StampCollectionQRCode $stampCollectionQRCode)
    {
        $this->qrCodes->add($stampCollectionQRCode);
    }

    /**
     * @param string $qrCodeId
     *
     * @return void
     */
    public function enableQRCode(string $qrCodeId)
    {
        $this->qrCodes->enable($qrCodeId);
    }

    /**
     * @param string $qrCodeId
     *
     * @return void
     */
    public function disableQRCode(string $qrCodeId)
    {
        $this->qrCodes->disable($qrCodeId);
    }

    /**
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function updateQrCode(string $qrCodeId, string $labelName, string $useType)
    {
        $this->qrCodes->update($qrCodeId, $labelName, $useType);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
