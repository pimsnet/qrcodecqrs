<?php

namespace PimsCQRS\Domain\Business\ReadModel;

use Broadway\ReadModel\Identifiable;
use Broadway\ReadModel\Repository;
use PimsCQRS\Domain\Business\Category\Category;

interface BusinessReadModelRepositoryInterface extends Repository
{

    /**
     * @param float $lat
     * @param float $long
     * @param int $from
     * @param int $size
     *
     * @return mixed
     */
    public function nearest(float $lat, float $long, int $from, int $size);

    /**
     * @param array $ids
     * @param float|null $lat
     * @param float|null $long
     *
     * @return mixed
     */
    public function findByIds(array $ids, float $lat = null, float $long = null);

    /**
     * @param string $id
     * @param float|null $lat
     * @param float|null $long
     *
     * @return mixed
     */
    public function findOneById(string $id, float $lat = null, float $long = null);

    /**
     * @param int $page
     * @param int $pageSize
     * @param array $sorted
     * @param string $filtered
     *
     * @return Identifiable[]
     */
    public function findAllWithPagination(int $page, int $pageSize, array $sorted, string $filtered);

    /**
     * @param Category $category
     * @param float|null $lat
     * @param float|null $long
     * @param int $from
     * @param int $size
     *
     * @return mixed
     */
    public function findAllByCategory(Category $category, float $lat = null, float $long = null, int $from = 0, int $size = 1000);
}