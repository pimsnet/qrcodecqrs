<?php

namespace PimsCQRS\Domain\Business\ReadModel;

use Broadway\Domain\DateTime;
use Broadway\ReadModel\SerializableReadModel;
use PimsCQRS\Domain\Business\StampCollectionQRCode;

interface BusinessInterface extends SerializableReadModel
{
    /**
     * @param string $businessId
     * @param string $userId
     * @param string $name
     * @param DateTime $dateTime
     *
     * @return Business
     */
    public static function createBusiness(
        string $businessId, string $userId, string $name, DateTime $dateTime
    );

    /**
     * @param StampCollectionQRCode $stampCollectionQRCode
     *
     * @return void
     */
    public function defineStampCollectionQRCode(StampCollectionQRCode $stampCollectionQRCode);

    /**
     * @param string $qrCodeId
     *
     * @return void
     */
    public function enableQRCode(string $qrCodeId);

    /**
     * @param string $qrCodeId
     *
     * @return void
     */
    public function disableQRCode(string $qrCodeId);

    /**
     * @param string $qrCodeId
     * @param string $labelName
     * @param string $useType
     *
     * @return void
     */
    public function updateQrCode(string $qrCodeId, string $labelName, string $useType);

    /**
     * @return string
     */
    public function getName();
}