<?php

namespace PimsCQRS\Domain\Event;

use Broadway\Serializer\Serializable;

interface SerializableDomainEvent extends Serializable
{
    public static function deserialize(array $data): SerializableDomainEvent;

    public function serialize(): array;
}