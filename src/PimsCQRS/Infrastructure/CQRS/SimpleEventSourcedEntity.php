<?php

namespace PimsCQRS\Infrastructure\CQRS;

use Broadway\Domain\DateTime;
use Broadway\EventSourcing\AggregateRootAlreadyRegisteredException;
use Broadway\EventSourcing\EventSourcedEntity;
use PimsCQRS\Domain\Event\SerializableDomainEvent;

/**
 * Convenience base class for event sourced entities.
 */
abstract class SimpleEventSourcedEntity implements EventSourcedEntity
{
    /**
     * @var EventSourcedAggregateRoot|null
     */
    public $aggregateRoot;

    /**
     * {@inheritDoc}
     */
    public function handleEventRecursively(SerializableDomainEvent $event, DateTime $dateTime)
    {
        $this->handleEvent($event, $dateTime);

        foreach ($this->getChildEntities() as $entity) {
            $entity->registerAggregateRoot($this->aggregateRoot);
            $entity->handleEventRecursively($event, $dateTime);
        }
    }

    /**
     * @param SerializableDomainEvent $event
     * @param DateTime $dateTime
     */
    protected function handleEvent(SerializableDomainEvent $event, DateTime $dateTime)
    {
        $method = $this->getApplyMethod($event);

        if (!method_exists($this, $method)) {
            return;
        }

        $this->$method($event, $dateTime);
    }

    /**
     * {@inheritDoc}
     */
    public function registerAggregateRoot(\Broadway\EventSourcing\EventSourcedAggregateRoot $aggregateRoot)
    {
        if (null !== $this->aggregateRoot && $this->aggregateRoot !== $aggregateRoot) {
            throw new AggregateRootAlreadyRegisteredException();
        }

        $this->aggregateRoot = $aggregateRoot;
    }

    private function getApplyMethod($event)
    {
        $classParts = explode('\\', get_class($event));

        return 'apply' . end($classParts);
    }

    protected function apply($event)
    {
        $this->aggregateRoot->apply($event);
    }

    /**
     * Recursively handles $event
     *
     * @param $event
     */
    public function handleRecursively($event)
    {
        // TODO: Implement handleRecursively() method.
    }

    protected function getChildEntities()
    {
        return [];
    }

    /**
     * @return null|EventSourcedAggregateRoot
     */
    public function getAggregateRoot(): ?EventSourcedAggregateRoot
    {
        return $this->aggregateRoot;
    }
}
