<?php

namespace PimsCQRS\Infrastructure\CQRS;

use Broadway\Domain\DomainEventStream;
use Broadway\EventHandling\EventListener;

/**
 * Simple synchronous publishing of events.
 */
class SimpleEventBus extends \Broadway\EventHandling\SimpleEventBus
{
    private $eventListeners = [];
    private $queue          = [];
    private $isPublishing   = false;

    /**
     * {@inheritDoc}
     */
    public function publish(DomainEventStream $domainMessages)
    {
        foreach ($domainMessages as $domainMessage) {
            $this->queue[] = $domainMessage;
        }

        if (! $this->isPublishing) {
            $this->isPublishing = true;

            try {
                while ($domainMessage = array_shift($this->queue)) {
                    if (true === in_array(DeprecatedEvent::class, class_implements($domainMessage->getPayload()))) {
                        continue;
                    }

                    foreach ($this->eventListeners as $eventListener) {
                        $eventListener->handle($domainMessage);
                    }
                }
            } finally {
                $this->isPublishing = false;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function subscribe(EventListener $eventListener)
    {
        $this->eventListeners[] = $eventListener;
    }
}
