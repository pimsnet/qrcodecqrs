<?php

namespace PimsCQRS\Infrastructure\CQRS;

use Broadway\Domain\DomainEventStream;

interface StreamSerializerInterface
{
    /**
     * Serializes a DomainEventStreamInterface into an array of php arrays
     * that represent the domain event messages.
     *
     * @param DomainEventStream $stream The event stream.
     * @return array                             An array of assoc arrays.
     */
    public function serialize(DomainEventStream $stream);
    /**
     * Hydrates a DomainEventStreamInterface out of database records in array
     * format representing the domain messages.
     *
     * @param array $records              The domain messages in an array
     *                                    format, as found in the database.
     * @return DomainEventStream The reconstituted domain event stream.
     */
    public function deserialize(array $records);
}
