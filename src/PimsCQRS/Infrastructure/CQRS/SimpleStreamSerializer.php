<?php

namespace PimsCQRS\Infrastructure\CQRS;

use Broadway\Domain\DateTime;
use Broadway\Domain\DomainEventStream;
use Broadway\Domain\DomainMessage;
use Broadway\Serializer\Serializer;

/**
 * Serves as convenient serializer for domain messages.
 * Essentially it groups the serialization / deserialization of the different
 * parts of a domain message.
 *
 * Class SimpleStreamSerializer
 * @package CminorIO\LaravelOnBroadway\EventStore\LaravelStore
 */
class SimpleStreamSerializer implements StreamSerializerInterface
{
    /** @var Serializer */
    private $payloadSerializer;

    /** @var Serializer */
    private $metadataSerializer;

    /**
     * @param Serializer $payloadSerializer
     * @param Serializer $metadataSerializer
     */
    public function __construct(
        Serializer $payloadSerializer,
        Serializer $metadataSerializer
    ) {
        $this->payloadSerializer = $payloadSerializer;
        $this->metadataSerializer = $metadataSerializer;
    }

    /**
     * @param DomainEventStream $stream
     *
     * @return array
     */
    public function serialize(DomainEventStream $stream)
    {
        $records = [];

        foreach ($stream as $message) {
            $records[] = $this->convertDomainMessage($message);
        }

        return $records;
    }

    /**
     * @param DomainMessage $message
     *
     * @return array
     */
    private function convertDomainMessage(DomainMessage $message)
    {
        $metadata = $this->metadataSerializer->serialize($message->getMetadata());
        $payload = $this->payloadSerializer->serialize($message->getPayload());

        return [
            'uuid' => (string)$message->getId(),
            'playhead' => $message->getPlayhead(),
            'metadata' => json_encode($metadata),
            'payload' => json_encode($payload),
            'recorded_on' => $message->getRecordedOn()->toString(),
            'type' => $message->getType()
        ];
    }

    /**
     * @param array $records
     *
     * @return DomainEventStream
     */
    public function deserialize(array $records)
    {
        $payloadSerializer = $this->payloadSerializer;
        $metaSerializer = $this->metadataSerializer;

        $messages = array_map(
            function ($row) use ($payloadSerializer, $metaSerializer) {
                // Unfortunately Broadway's default implementation require this.
                return new DomainMessage(
                    $row['uuid'],
                    $row['playhead'],
                    $metaSerializer->deserialize(
                        json_decode($row['metadata'], true)
                    ),
                    $payloadSerializer->deserialize(
                        json_decode($row['payload'], true)
                    ),
                    DateTime::fromString($row['recorded_on'])
                );
            },
            $records
        );

        return new DomainEventStream($messages);
    }
}
