<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use PimsCQRS\Domain\User\Role\BusinessAdminRole;
use PimsCQRS\Domain\User\Role\GodRole;
use PimsCQRS\Domain\User\Role\Role;
use PimsCQRS\Domain\User\Role\RoleFactory;
use PimsCQRS\Domain\User\Role\UserRole;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class RolesMiddleware extends BaseMiddleware
{
    public function handle($request, Closure $next, string $role = null)
    {
        try {
            if (!$token = $this->auth->setRequest($request)->getToken()) {
                return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
            }
        } catch (\Exception $exception) {
            return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
        }

        try {
            $user = $this->auth->toUser($token);
        } catch (TokenExpiredException $e) {
            return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
        } catch (JWTException $e) {
            return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
        }

        if (!$user) {
            return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
        }

        if ($role !== null) {
            if (false === $user->roles->contains(new GodRole()) && false === $user->roles->contains($this->getRoleToValidate($role, $request))) {
                return $this->respond('tymon.jwt.invalid', 'access_forbidden', 403, 'Unauthorized');
            }
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }

    /**
     * @param string $role
     * @param Request $request
     *
     * @return Role
     */
    private function getRoleToValidate(string $role, Request $request): Role
    {
        return RoleFactory::deserialize(
            $role,
            $request->route()->parameters()
        );
    }
}