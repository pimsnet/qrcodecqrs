<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\CommandHandler;
use Broadway\CommandHandling\Testing\CommandHandlerScenarioTestCase;
use Broadway\EventHandling\EventBus;
use Broadway\EventStore\EventStore;
use PimsCQRS\Domain\Business\BusinessRepository;
use PimsCQRS\Domain\Business\Event\BusinessCreatedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDefinedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDisabledEvent;
use PimsCQRS\Domain\Business\QRCode;
use PimsCQRS\Domain\Business\StampCollectionQRCode;
use Ramsey\Uuid\Uuid;

class DisableQRCodeCommandHandlerTest extends CommandHandlerScenarioTestCase
{
    public function test_DisableQRCodeCommandHandler_should_disable_qrCode_on_success()
    {
        $qrCodeId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $this->scenario
            ->withAggregateId($businessId)
            ->given([
                new BusinessCreatedEvent($businessId, $userId, 'name'),
                new StampCollectionQRCodeDefinedEvent($businessId, $userId, $qrCode)
            ])
            ->when(new DisableQRCodeCommand($businessId, $userId, $qrCodeId))
            ->then([new StampCollectionQRCodeDisabledEvent($businessId, $userId, $qrCodeId)]);
    }

    /**
     * Create a command handler for the given scenario test case.
     *
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     *
     * @return CommandHandler
     */
    protected function createCommandHandler(EventStore $eventStore, EventBus $eventBus)
    {
        return new DisableQRCodeCommandHandler(
            new BusinessRepository($eventStore, $eventBus)
        );
    }
}
