<?php

namespace PimsCQRS\Domain\Business\Command;

use Broadway\CommandHandling\CommandHandler;
use Broadway\CommandHandling\Testing\CommandHandlerScenarioTestCase;
use Broadway\EventHandling\EventBus;
use Broadway\EventStore\EventStore;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDefinedEvent;
use PimsCQRS\Domain\Business\QRCode;
use PimsCQRS\Domain\Business\StampCollectionQRCode;
use Ramsey\Uuid\Uuid;
use PimsCQRS\Domain\Business\BusinessRepository;
use PimsCQRS\Domain\Business\Event\BusinessCreatedEvent;

class CreateBusinessCommandHandlerTest extends CommandHandlerScenarioTestCase
{
    public function test_handleCreateBusinessEvent_should_save_created_business_event()
    {
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();
        $name = 'test business name';
        $qrCodeId = Uuid::uuid4()->toString();
        $labelName = 'qr code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $this->scenario
            ->withAggregateId($businessId)
            ->given([])
            ->when(new CreateBusinessCommand($businessId, $userId, $name, $qrCode))
            ->then([
                new BusinessCreatedEvent($businessId, $userId, $name),
                new StampCollectionQRCodeDefinedEvent($businessId, $userId, $qrCode)
            ]);
    }

    /**
     * Create a command handler for the given scenario test case.
     *
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     *
     * @return CommandHandler
     */
    protected function createCommandHandler(EventStore $eventStore, EventBus $eventBus)
    {
        return new CreateBusinessCommandHandler(
            new BusinessRepository($eventStore, $eventBus)
        );
    }
}
