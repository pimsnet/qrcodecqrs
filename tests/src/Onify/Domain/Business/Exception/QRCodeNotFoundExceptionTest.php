<?php

namespace PimsCQRS\Domain\Business\Exception;

class QRCodeNotFoundExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function test_QRCodeNotFoundException_should_throw_exception()
    {
        $this->expectException(QRCodeNotFoundException::class);
        throw new QRCodeNotFoundException();
    }
}
