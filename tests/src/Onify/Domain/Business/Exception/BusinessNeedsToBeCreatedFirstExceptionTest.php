<?php

namespace PimsCQRS\Domain\Business\Exception;

class BusinessNeedsToBeCreatedFirstExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function test_BusinessNeedsToBeCreatedFirstException_should_throw_exception()
    {
        $this->expectException(BusinessNeedsToBeCreatedFirstException::class);
        throw new BusinessNeedsToBeCreatedFirstException();
    }
}
