<?php

namespace PimsCQRS\Domain\Business\Exception;

class QRCodeExistsExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function test_QRCodeExistsException_should_throw_exception()
    {
        $this->expectException(QRCodeExistsException::class);
        throw new QRCodeExistsException();
    }
}
