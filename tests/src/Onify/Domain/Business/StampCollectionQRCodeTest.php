<?php

namespace PimsCQRS\Domain\Business;

use Ramsey\Uuid\Uuid;

class StampCollectionQRCodeTest extends \PHPUnit_Framework_TestCase
{
    private $data;

    private $qrCode;

    private $serialized;

    protected function setUp()
    {
        $qrCodeId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $this->data = [
            'qrCodeId' => $qrCodeId,
            'businessId' => $businessId,
            'userId' => $userId,
            'labelName' => $labelName,
            'useType' => $useType,
            'isEnabled' => $isEnabled
        ];

        $this->qrCode = new StampCollectionQRCode(
            $this->data['qrCodeId'],
            $this->data['businessId'],
            $this->data['userId'],
            $this->data['labelName'],
            $this->data['useType'],
            $this->data['isEnabled']
        );

        $this->serialized = $this->qrCode->serialize();

        parent::setUp();
    }

    public function tearDown()
    {
        $this->data = null;
        $this->qrCode = null;
        $this->serialized = null;
    }

    public function test_serialize_should_return_array()
    {
        $this->assertEquals($this->data, $this->serialized);
    }

    public function test_deserialize_should_return_object()
    {
        $deserialized = StampCollectionQRCode::deserialize($this->serialized);
        $this->assertEquals($deserialized, $this->qrCode);
    }
}
