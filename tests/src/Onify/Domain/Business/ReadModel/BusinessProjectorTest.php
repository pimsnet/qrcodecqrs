<?php

namespace PimsCQRS\WebPlugin\Business\ReadModel;

use Broadway\Domain\DateTime;
use Broadway\ReadModel\InMemory\InMemoryRepository;
use Broadway\ReadModel\Projector;
use Broadway\ReadModel\Testing\ProjectorScenarioTestCase;
use PimsCQRS\Domain\Business\Event\BusinessCreatedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDisabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeEnabledEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeUpdatedEvent;
use PimsCQRS\Domain\Business\Event\StampCollectionQRCodeDefinedEvent;
use PimsCQRS\Domain\Business\QRCode;
use PimsCQRS\Domain\Business\ReadModel\Business;
use PimsCQRS\Domain\Business\ReadModel\BusinessProjector;
use PimsCQRS\Domain\Business\StampCollectionQRCode;
use Ramsey\Uuid\Uuid;

class BusinessProjectorTest extends ProjectorScenarioTestCase
{
    /**
     * @param InMemoryRepository $repository
     *
     * @return Projector
     */
    protected function createProjector(InMemoryRepository $repository)
    {
        return new BusinessProjector($repository);
    }

    public function test_applyBusinessCreatedEvent_should_create_new_business()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $name = 'Test Business';
        $dateTime = DateTime::now();

        $this->scenario
            ->given([])
            ->withDateTimeGenerator(function ($event) use ($dateTime) {
                return $dateTime;
            })
            ->when(new BusinessCreatedEvent($businessId, $userId, $name))
            ->then([
                $this->createBusiness($businessId, $userId, $name, $dateTime)
            ]);
    }

    public function test_applyStampCollectionQRCodeDefinedEvent_should_add_StampCollectionQRCode()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();

        $name = 'Test Business';
        $dateTime = DateTime::now();
        $business = $this->createBusiness(
            $businessId, $userId, $name, $dateTime
        );
        $qrCodeId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $business->defineStampCollectionQRCode($qrCode);

        $this->scenario
            ->withDateTimeGenerator(function ($event) use ($dateTime) {
                return $dateTime;
            })
            ->given([
                new BusinessCreatedEvent($businessId, $userId, $name),
            ])
            ->when(new StampCollectionQRCodeDefinedEvent($businessId, $userId, $qrCode))
            ->then([$business]);
    }

    public function test_applyStampCollectionQRCodeEnabledEvent_should_enable_StampCollectionQRCode()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();

        $name = 'Test Business';
        $dateTime = DateTime::now();
        $business = $this->createBusiness(
            $businessId, $userId, $name, $dateTime
        );
        $qrCodeId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = false;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $business->defineStampCollectionQRCode($qrCode);
        $business->enableQRCode($qrCodeId);

        $this->scenario
            ->withDateTimeGenerator(function ($event) use ($dateTime) {
                return $dateTime;
            })
            ->given([
                new BusinessCreatedEvent($businessId, $userId, $name),
                new StampCollectionQRCodeDefinedEvent($businessId, $userId, $qrCode)
            ])
            ->when(new StampCollectionQRCodeEnabledEvent($businessId, $userId, $qrCodeId))
            ->then([$business]);
    }

    public function test_applyStampCollectionQRCodeDisabledEvent_should_disable_StampCollectionQRCode()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();

        $name = 'Test Business';
        $dateTime = DateTime::now();
        $business = $this->createBusiness(
            $businessId, $userId, $name, $dateTime
        );
        $qrCodeId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $business->defineStampCollectionQRCode($qrCode);
        $business->disableQRCode($qrCodeId);

        $this->scenario
            ->withDateTimeGenerator(function ($event) use ($dateTime) {
                return $dateTime;
            })
            ->given([
                new BusinessCreatedEvent($businessId, $userId, $name),
                new StampCollectionQRCodeDefinedEvent($businessId, $userId, $qrCode)
            ])
            ->when(new StampCollectionQRCodeDisabledEvent($businessId, $userId, $qrCodeId))
            ->then([$business]);
    }

    public function test_applyStampCollectionQRCodeUpdatedEvent_should_update_StampCollectionQRCode()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();

        $name = 'Test Business';
        $dateTime = DateTime::now();
        $business = $this->createBusiness(
            $businessId, $userId, $name, $dateTime
        );
        $qrCodeId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $newLabelName = 'new QRCode Label';
        $newUseType = QRCode::USE_ONCE;

        $business->defineStampCollectionQRCode($qrCode);
        $business->updateQrCode($qrCodeId, $newLabelName, $newUseType);

        $this->scenario
            ->withDateTimeGenerator(function ($event) use ($dateTime) {
                return $dateTime;
            })
            ->given([
                new BusinessCreatedEvent($businessId, $userId, $name),
                new StampCollectionQRCodeDefinedEvent($businessId, $userId, $qrCode)
            ])
            ->when(new StampCollectionQRCodeUpdatedEvent($businessId, $userId, $qrCodeId, $newLabelName, $newUseType))
            ->then([$business]);
    }

    private function createBusiness($businessId, $userId, $name, $dateTime)
    {
        return Business::createBusiness(
            $businessId, $userId, $name, $dateTime
        );
    }
}
