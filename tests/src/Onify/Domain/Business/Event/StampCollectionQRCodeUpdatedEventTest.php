<?php

namespace PimsCQRS\Domain\Business\Event;


use PimsCQRS\Domain\Business\QRCode;
use Ramsey\Uuid\Uuid;

class StampCollectionQRCodeUpdatedEventTest extends \PHPUnit_Framework_TestCase
{
    private $data;

    private $qrCodeUpdatedEvent;

    private $serialized;

    protected function setUp()
    {
        $qrCodeId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;

        $this->data = [
            'businessId' => $businessId,
            'userId' => $userId,
            'qrCodeId' => $qrCodeId,
            'labelName' => $labelName,
            'useType' => $useType
        ];

        $this->qrCodeUpdatedEvent = new StampCollectionQRCodeUpdatedEvent(
            $this->data['businessId'],
            $this->data['userId'],
            $this->data['qrCodeId'],
            $this->data['labelName'],
            $this->data['useType']
        );

        $this->serialized = $this->qrCodeUpdatedEvent->serialize();

        parent::setUp();
    }

    public function tearDown()
    {
        $this->data = null;
        $this->qrCodeUpdatedEvent = null;
        $this->serialized = null;
    }

    public function test_serialize_should_return_array()
    {
        $this->assertEquals($this->data, $this->serialized);
    }

    public function test_deserialize_should_return_object()
    {
        $deserialized = StampCollectionQRCodeUpdatedEvent::deserialize($this->serialized);
        $this->assertEquals($deserialized, $this->qrCodeUpdatedEvent);
    }
}
