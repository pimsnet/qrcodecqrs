<?php

namespace PimsCQRS\Domain\Business\Event;

use PimsCQRS\Domain\Business\QRCode;
use PimsCQRS\Domain\Business\StampCollectionQRCode;
use Ramsey\Uuid\Uuid;

class StampCollectionQRCodeDefinedEventTest extends \PHPUnit_Framework_TestCase
{
    private $data;

    private $qrCodeDefinedEvent;

    private $serialized;

    protected function setUp()
    {
        $qrCodeId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $qrCode = new StampCollectionQRCode($qrCodeId, $businessId, $userId, $labelName, $useType, $isEnabled);

        $this->data = [
            'businessId' => $businessId,
            'userId' => $userId,
            'qrCode' => $qrCode->serialize()
        ];

        $this->qrCodeDefinedEvent = new StampCollectionQRCodeDefinedEvent(
            $this->data['businessId'],
            $this->data['userId'],
            StampCollectionQRCode::deserialize($this->data['qrCode'])
        );

        $this->serialized = $this->qrCodeDefinedEvent->serialize();

        parent::setUp();
    }

    public function tearDown()
    {
        $this->data = null;
        $this->qrCodeDefinedEvent = null;
        $this->serialized = null;
    }

    public function test_serialize_should_return_array()
    {
        $this->assertEquals($this->data, $this->serialized);
    }

    public function test_deserialize_should_return_object()
    {
        $deserialized = StampCollectionQRCodeDefinedEvent::deserialize($this->serialized);
        $this->assertEquals($deserialized, $this->qrCodeDefinedEvent);
    }
}
