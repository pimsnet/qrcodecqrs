<?php

namespace PimsCQRS\Domain\Business\Event;

use Ramsey\Uuid\Uuid;

class StampCollectionQRCodeDisabledEventTest extends \PHPUnit_Framework_TestCase
{
    private $data;

    private $qrCodeDisabledEvent;

    private $serialized;

    protected function setUp()
    {
        $qrCodeId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();

        $this->data = [
            'businessId' => $businessId,
            'userId' => $userId,
            'qrCodeId' => $qrCodeId
        ];

        $this->qrCodeDisabledEvent = new StampCollectionQRCodeDisabledEvent(
            $this->data['businessId'],
            $this->data['userId'],
            $this->data['qrCodeId']
        );

        $this->serialized = $this->qrCodeDisabledEvent->serialize();

        parent::setUp();
    }

    public function tearDown()
    {
        $this->data = null;
        $this->qrCodeDisabledEvent = null;
        $this->serialized = null;
    }

    public function test_serialize_should_return_array()
    {
        $this->assertEquals($this->data, $this->serialized);
    }

    public function test_deserialize_should_return_object()
    {
        $deserialized = StampCollectionQRCodeDisabledEvent::deserialize($this->serialized);
        $this->assertEquals($deserialized, $this->qrCodeDisabledEvent);
    }
}
