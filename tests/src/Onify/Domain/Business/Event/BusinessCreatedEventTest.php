<?php

namespace PimsCQRS\Domain\Business\Event;

use Ramsey\Uuid\Uuid;

class BusinessCreatedEventTest extends \PHPUnit_Framework_TestCase
{

    public function test_serialize_should_return_serialized_object()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $name = 'Test Business';

        $event = new BusinessCreatedEvent($businessId, $userId, $name);
        $serialized = $event->serialize();

        $this->assertEquals($businessId, $serialized['businessId']);
        $this->assertEquals($userId, $serialized['userId']);
        $this->assertEquals($name, $serialized['name']);
    }

    public function test_deserialize_should_deserialize_object()
    {
        $userId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $name = 'Test Business';

        $event = new BusinessCreatedEvent($businessId, $userId, $name);
        $serialized = $event->serialize();
        $deserialized = BusinessCreatedEvent::deserialize($serialized);

        $this->assertEquals($deserialized, $event);
    }
}
