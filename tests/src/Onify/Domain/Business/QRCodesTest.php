<?php

namespace PimsCQRS\Domain\Business;

use PimsCQRS\Domain\Business\Exception\QRCodeExistsException;
use Ramsey\Uuid\Uuid;

class QRCodesTest extends \PHPUnit_Framework_TestCase
{
    private $qrCode;

    private $qrCodeData;

    protected function setUp()
    {
        $qrCodeId = Uuid::uuid4()->toString();
        $businessId = Uuid::uuid4()->toString();
        $userId = Uuid::uuid4()->toString();
        $labelName = 'QR Code';
        $useType = QRCode::USE_MANY;
        $isEnabled = true;

        $this->qrCodeData = [
            'qrCodeId' => $qrCodeId,
            'businessId' => $businessId,
            'userId' => $userId,
            'labelName' => $labelName,
            'useType' => $useType,
            'isEnabled' => $isEnabled
        ];

        $this->qrCode = new StampCollectionQRCode(
            $this->qrCodeData['qrCodeId'],
            $this->qrCodeData['businessId'],
            $this->qrCodeData['userId'],
            $this->qrCodeData['labelName'],
            $this->qrCodeData['useType'],
            $this->qrCodeData['isEnabled']
        );

        parent::setUp();
    }

    public function test_containsID_should_return_true_if_QRCode_is_in_collection()
    {
        $qrCodesCollection = new QRCodes($this->qrCode);

        $this->assertTrue($qrCodesCollection->containsId($this->qrCodeData['qrCodeId']));
    }

    public function test_containsID_should_return_false_when_QRCode_is_not_in_collection()
    {
        $qrCodesCollection = new QRCodes($this->qrCode);

        $this->assertFalse($qrCodesCollection->containsId('bad qr code id'));
    }

    public function test_add_should_add_QRCode_to_collection()
    {
        $qrCodesCollection = new QRCodes();
        $qrCodesCollection->add($this->qrCode);

        $this->assertTrue($qrCodesCollection->containsId($this->qrCodeData['qrCodeId']));
    }

    public function test_add_should_throw_exception_when_adding_duplicate_QRCode_to_collection()
    {
        $qrCodesCollection = new QRCodes($this->qrCode);

        $this->expectException(QRCodeExistsException::class);

        $qrCodesCollection->add($this->qrCode);
    }
}
