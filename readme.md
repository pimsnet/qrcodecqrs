# QR Code sample code

Sample code from a past project made using:
- chunks of Laravel
- [CQRS](https://martinfowler.com/bliki/CQRS.html)
- [Broadway](https://github.com/broadway/broadway)